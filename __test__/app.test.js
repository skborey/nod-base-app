const request = require("supertest");

const app = require("../app.js");

describe("GET /", function () {
  it("responds html file", function (done) {
    request(app)
      .get("/")
      .set("Accept", "application/json")
      .expect("Content-Type", /text\/html/)
      .expect(200, done);
  });
});

describe("GET /users", function () {
  it("responds json file", function (done) {
    request(app)
      .get("/users")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });

  it("responds body with correct fields", function (done) {
    request(app)
      .get("/users")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.code).toBe(200);
        expect(response.body.message).toBe("Success");
        expect(response.body).toHaveProperty("data");
        done();
      })
      .catch((err) => done(err));
  });

  it("responds body with empty data when there is no user", function (done) {
    request(app)
      .get("/users")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.data.length).toBe(0);
        done();
      })
      .catch((err) => done(err));
  });
});
